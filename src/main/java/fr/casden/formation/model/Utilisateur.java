package fr.casden.formation.model;


public class Utilisateur {
    private String nom;
    private String prenom;
    private int age;

   public Utilisateur(String nom, String prenom, int age){
       this.nom = nom;
       this.prenom = prenom;
       this.age = age;
   }
    
    public String getNom(){
        return nom.toUpperCase();
    }

    public String getPrenom(){
        return prenom;
    }

    public int getAge() {
        //renvoie l'age
        return age;
    }

/*    public void setAge(int age){
        this.age=age;
    }
*/
    /**
     * Vieillit l'utilisateur d'un nombre d'années. Attention, l'age max est fixé à 100 ans.
     * @param nbAnnees entier positif
     * @return le nouvel age de l'utilisateur
     */    public  int vieillir(int nbAnnees){
        if (nbAnnees<=0) { 
            return age; 
        }

        if (age + nbAnnees>100) {
            age = 100;
            return 100;
        }

        age = age + nbAnnees;
        return age;

    }


    @Override
    public String toString() {
        return String.format("%s %s %d", prenom, nom, age);
    }

}
