package fr.casden.formation;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import fr.casden.formation.model.Utilisateur;

/**
 * Hello world!
 *
 */
public class App 
{
    public  static void main( String[] args )
    {
        for(int i=0; i < args.length; i=i+1) {
            System.out.println(args[i]);    
        }
        

        Utilisateur user ;
        user = new Utilisateur("remy", "Manu", 53);
        
        //user.setAge(user.getAge()+1);

        user.vieillir(20);

        System.out.println(user.toString());

        user.vieillir(30);

        System.getLogger(App.class.getName()).log(Level.INFO, user);
        
        
        System.out.printf("Hello %s %s %d !\n", user.getPrenom(), user.getNom(), user.getAge());


        System.out.println( "Hello World!" );
    }
}
